#SLNavigationSlide
这是一套可容纳多个ViewController的框架。显示效果分为标题和控制器视图，开放接口为NSArray，简单易用。 

```
@property (nonatomic, strong) NSArray *contents;// 子控制器
@property (nonatomic, strong) NSArray *titles;// 标题。标题数量应该和控制器数量一致
```