//
//  ViewController.m
//  SLNavigationSlide
//
//  Created by 深蓝 on 14/4/4.
//  Copyright (c) 2014年 SL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Extension)

/** 计算文本所占的矩阵范围*/
+ (CGSize)textSize:(NSString *)text maxSize:(CGSize)maxSize font:(UIFont *)font;

/** 返回View的xml结构*/
- (NSString *)digView:(UIView *)view;

@end
