//
//  ViewController.m
//  SLNavigationSlide
//
//  Created by 深蓝 on 14/4/4.
//  Copyright (c) 2014年 SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLNavigationSlideViewController : UIViewController

/** 参数一:存储顶部标题。参数二:存储底部控制器 */
+ (instancetype)navigationSlideWithTitles:(NSArray *)titles contents:(NSArray *)contents;

/** 顶部标题 */
@property (nonatomic, strong) NSArray *titles;

/** 底部控制器 */
@property (nonatomic, strong) NSArray *contents;

/** 标题间距 */
@property (nonatomic, assign) CGFloat margin;

/** 标题字体 */
@property (nonatomic, strong) UIFont *font;

@end
