//
//  ViewController.m
//  SLNavigationSlide
//
//  Created by 深蓝 on 14/4/4.
//  Copyright (c) 2014年 SL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideNavigationLabel : UILabel
@property (nonatomic, assign) CGFloat scale;
@end
