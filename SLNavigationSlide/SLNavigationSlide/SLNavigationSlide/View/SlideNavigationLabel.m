//
//  ViewController.m
//  SLNavigationSlide
//
//  Created by 深蓝 on 14/4/4.
//  Copyright (c) 2014年 SL. All rights reserved.
//

#define SLScale(scale) ((scale) * 0.2 + 0.8)

#import "SlideNavigationLabel.h"

@implementation SlideNavigationLabel

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.textColor = [UIColor redColor];
        self.textAlignment = NSTextAlignmentCenter;
        self.userInteractionEnabled = YES;
        _scale = 0.0;
    }
    return self;
}

- (void)setScale:(CGFloat)scale {
    _scale = scale;
    
    self.transform = CGAffineTransformMakeScale(SLScale(scale), SLScale(scale));
    self.textColor = [UIColor colorWithRed:scale green:0 blue:1-scale alpha:1.0];
}

@end
