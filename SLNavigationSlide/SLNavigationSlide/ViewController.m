//
//  ViewController.m
//  SLNavigationSlide
//
//  Created by 伟仔c on 15/4/6.
//  Copyright (c) 2015年 SL. All rights reserved.
//

#import "ViewController.h"
#import "SLNavigationSlideViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *titles = @[@"标题一", @"标题二", @"标题三", @"标题四", @"标题五", @"标题六", @"标题七", @"标题八", @"标题九", @"标题十"];
    
    UIViewController *vc1 = [[UIViewController alloc] init];
    UIViewController *vc2 = [[UIViewController alloc] init];
    UIViewController *vc3 = [[UIViewController alloc] init];
    UIViewController *vc4 = [[UIViewController alloc] init];
    UIViewController *vc5 = [[UIViewController alloc] init];
    UIViewController *vc6 = [[UIViewController alloc] init];
    UIViewController *vc7 = [[UIViewController alloc] init];
    UIViewController *vc8 = [[UIViewController alloc] init];
    UIViewController *vc9 = [[UIViewController alloc] init];
    UIViewController *vc10 = [[UIViewController alloc] init];
    NSArray *contents = @[vc1, vc2, vc3, vc4, vc5, vc6, vc7, vc8, vc9, vc10];
    
    /*****核心代码*****/
    SLNavigationSlideViewController *vc = [SLNavigationSlideViewController navigationSlideWithTitles:titles contents:contents];
    vc.view.frame = self.view.frame;
    vc.margin = 10;
    vc.font = [UIFont systemFontOfSize:18];
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    /*****核心代码*****/
}

@end
