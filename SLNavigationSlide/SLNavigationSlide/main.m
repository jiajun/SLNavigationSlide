//
//  main.m
//  SLNavigationSlide
//
//  Created by 伟仔c on 15/4/6.
//  Copyright (c) 2015年 SL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
